
import java.util.*;
import java.io.File;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program lists files and subdirectories of user-specified directories.");
        
        System.out.println();
    }
    public static void directoryInfo()
    {
 
         //initialize variables, create Scanner object, capture user input
        Scanner sc = new Scanner(System.in);
        String myDirectory = "";

        System.out.print("Please enter directory path: ");
        myDirectory = sc.nextLine();

        File directoryPath = new File(myDirectory);

        try{
      
        File listFile[] = directoryPath.listFiles();

        for(int i =0; i<listFile.length; i++){
        System.out.println("\nName: "+listFile[i].getName());
        System.out.println("Path: "+listFile[i].getAbsolutePath());
        System.out.println("Size (Bytes): "+listFile[i].length());
        System.out.println("Size (KB): "+listFile[i].length()/1024);
        System.out.println("Size (MB): "+listFile[i].length()/(1024*1024));
        }
    }
    catch (Exception e){
        System.err.println(e.getMessage());
    }
     sc.close();    
               
   }     
}

