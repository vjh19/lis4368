> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Web Applications Development 

## Victoria Hagen

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Apache Tomcat
    - Install Git
    - Git clone assignment starter files.
    - Complete BitbucketStationLocations tutroiral and create a course bitbucket. Provide read only access.
    - Write Git commands with descrption.

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Complete Tomcat tutorial.
    - Provide screenshots of at least querybook.html, query results, and a2/index.jsp.
    - Provide links Assessment links.
    - Provide skillsets 1-3.

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a entity Relationship Diagram (ERD) following the buisness rules.
    - Include data (at leat 10 records each table).
    - Include ERD screenshot, a3.mwb, and a3.sql.
    - Provide skillsets 4-6.

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Make changes to thanks.jsp, customerform.jsp, Customer.java, and CustomerServlet.java to add server-side validation and form data.
    - Compile Customer.java and CustomerServlet.java.
    - Provide screenshots of  both Failed and Passed Validation.
    - Provide skillsets 10-12.

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create ConnectionPool.java, CustomerDB.java, and DBUtil.java.
    - Compile all servlets. 
    - Provide screenshots of custoemrform.jsp, thanks.jsp, and database entry.
    - Provide skillsets 13-15.
    
6. [P1 README.md](p1/README.md "My P1 README.md file")
    -  Add the following jQuery validation and regular expressions-- as per the entity attribute requirements.
    - Use regexp to only allow appropriate characters for each control
    - Provide screenshots of Main/Splash Page both Failed and Passed Validation.
    - Provide skillsets 7-9.
    
7. [P2 README.md](p2/README.md "My P2 README.md file")
    -  Complete the JSP/Servlets web application using the MVC framework and providing create, read, update and delete (CRUD) functionality- adds the edit and delete buttons.
    - Compile the servlets.
    - Provide screenshots of the database and web pages for insert, update, and delete. 
    
