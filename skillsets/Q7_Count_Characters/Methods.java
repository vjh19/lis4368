
import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("\t1.Counts number and types of characters from user-entered string.\n\t2.Count: total, letters (upper-/lower-case), numbers, spaces, and other characters.\n\tHint: Helpful methods: isLetter(), isDigit(), isSpaceChar(), and others. ;)");

        
        System.out.println();
    }
    public static void countCharacters()
    {
 
         //initialize variables, create Scanner object, capture user input
        Scanner sc = new Scanner(System.in);
        String userInput = "";
        int letter = 0;
        int upper = 0;
        int lower = 0;
        int num = 0;
        int space = 0;
        int other = 0;

        
        System.out.print("Please enter string: ");
        userInput= sc.nextLine(); 
       char[] ch = userInput.toCharArray();
       
       for(int i =0; i < userInput.length(); i++){
           if(Character.isLetter(ch[i]))
           {
               if(Character.isUpperCase(ch[i])){
                   upper++;
               }
               if(Character.isLowerCase(ch[i])){
                lower++;
                }
                letter++;
           }
           else if(Character.isDigit(ch[i])){
               num++;
           }
           else if(Character.isSpaceChar(ch[i])){
               space++;
           }
           else{
               other++;
           }
        }  
        System.out.println("\nYour string: \""+userInput+"\" has the following number and types of characters:");
        System.out.println("Total number of characters: "+ userInput.length());
        System.out.println("Letter(s): "+ letter);
        System.out.println("Upper-case letter(s): "+ upper);
        System.out.println("Lower-case letter(s): "+ lower);
        System.out.println("Number(s): "+ num);
        System.out.println("Space(s): "+ space);
        System.out.println("Other character(s): "+ other);
      sc.close();
    
}
}
