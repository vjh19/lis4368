> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development 

## Victoria Hagen

### Assignment 1 Requirements:

*Five parts:*

1. Install AMPPS, JDK, Apache Tomcat, and Git.
2. Complete BitcuketStationLocations turorial and create a course bitbucket repository. Provide read-only access and links.
3. Clone assignment starter files with Git and fill out A1 index.jsp.
4. Write Git commands with short description.
5. Screenshots of JDK, Tomcat and A1 index.jsp.

#### README.md file should include the following items:

* Screenshot of JDK Hello.java and Tomcat
* Screenshot of A1 index.jsp
* Git commands and descriptions
* Link to BitbucketStationLocations and LIS4381 repo.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Tomcat running:* 

[<img src = "img/tomcat.png">](http://localhost:9999/)

*Screenshot of A1 index.jsp:*
[<img src = "img/a1index.png">](http://localhost:9999/lis4368/a1/index.jsp)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/vjh19/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*LIS4381 repository:*
[A1 LIS4368 Repos Link](https://bitbucket.org/vjh19/lis4368/src/master/ "LIS4368 Repo")

*A1 index.jsp: *
[A1 LIS4368 Link](http://localhost:9999/lis4368/a1/index.jsp "LIS4368 A1 index.jsp")


