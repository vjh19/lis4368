> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development 

## Victoria Hagen

### Assignment 2 Requirements:

*Four parts:*

1. Complete Tomcat tutorial.
2. Provide screenshots of at least querybook.html, query results, and a2/index.jsp
3. Provide links Assesment links.
4. Provide skillsets 1-3.


#### README.md file should include the following items:

* Screenshot of querybook.html, query results, and a2/index.jsp
* Screenshots of /hello and /sayhello pages
* SS1-3
* Assessment links


#### Assignment Screenshots:
#### \*Click Images for Assessment Links\*

*Screenshot of index.html*:

[<img src = "img/hello.png">](http://localhost:9999/hello)

*Screenshot of sayhello:* 

[<img src = "img/using_servlets.png">](http://localhost:9999/hello/sayhello)

*Screenshot of querybook.html (Selected Item):*

[<img src = "img/database_connectivity1.png">](http://localhost:9999/hello/querybook.html)

*Screenshot of Query Results:*

![Query Results](img/database_connectivity2.png)

*Screenshot of a2/index.jsp (Selected Item):*

[<img src = "img/a2.png">](http://localhost:9999/lis4368/a2/index.jsp)

#### Skillset Screenshots:
| *Screenshot of SS1*:  | *Screenshot of SS2*:  |  *Screenshot of SS3*: |
|---|---|---|
| ![SS1 Screenshot](img/ss1.png)  | ![SS2 Screenshot](img/ss2.png)  |    ![SS3 Screenshot](img/ss3.png)  |

#### Assessment Links:

*http://localhost:9999/hello:*
[Hello](http://localhost:9999/hello "Hello")

*http://localhost:9999/hello/sayhello:*
[Say Hello](http://localhost:9999/hello/sayhello "Say hello")

*http://localhost:9999/hello/querybook.html:*
[Query](http://localhost:9999/hello/querybook.html "Query")
