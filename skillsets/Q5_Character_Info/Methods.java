
import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program determines total number of characters in line of text, as well as number of times specific character is used.\nProgram displays character's ASCII value");
        System.out.println();
        System.out.println("References: \nASCII Background: https://en.wikipedia.org/wiki/ASCII\nASCII Character Table: https://www.ascii-code.com/\nLookup Tables: https://www.lookuptables.com/");
        
        System.out.println();

    }
    public static void characterInfo()
    {
 
         //initialize variables, create Scanner object, capture user input
        String text = "";
        char value = 0;
        Scanner sc = new Scanner(System.in);
        
        for(int i=1; i>0; i++){
        System.out.println("//Run #"+i+":");
        System.out.print("Please enter line of text: ");
        if(i>1){
        text = sc.nextLine();
        text = sc.nextLine();
        }
        else{
            text = sc.nextLine();
        }
        System.out.print("Please enter character to check: ");
        value = sc.next().charAt(0);
        System.out.println();
        int ascii = (int)value;
        int count = 0;
        for(int j=0; j<text.length(); j++){
            if (text.charAt(j) == value) {
                count++;
            }
        }
        System.out.println("Number of characters in line of text: "+text.chars().count());
        System.out.println("The character " + value + " appears " + count +" time(s) in line of text.");
        System.out.println("ASCII value: " + ascii);
    System.out.println();   
    }
     sc.close();    
               
   }     
}

