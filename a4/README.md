> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development 

## Victoria Hagen

### Assignment 4 Requirements:

*Four Parts:*

1. Make changes to thanks.jsp, customerform.jsp, Customer.java, and CustomerServlet.java to add server-side validation and form data.
2. Compile Customer.java and CustomerServlet.java.
3. Provide screenshots of both Failed and Passed Validation.
4. Provide skillsets 10-12.

#### README.md file should include the following items:

* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Screenshots of skillsets 10-12

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![JFailed Validation Screenshot](img/failed.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)

#### Skillset Screenshots:
| *Screenshot of SS10*:  | *Screenshot of SS11*: |*Screenshot of SS12*: |
|---|---|---|
| ![SS10 Screenshot](img/ss10.png)  | ![SS11 Screenshot](img/ss11.png)  |  ![SS12 Screenshot](img/ss12.png) |
  


