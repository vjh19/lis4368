> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development 

## Victoria Hagen

### Project 1 Requirements:

*Four Parts:*

1. Add the following jQuery validation and regular expressions-- as per the entity attribute requirements
2. Use regexp to only allow appropriate characters for each control
3. Provide screenshots of Main/Splash Page both Failed and Passed Validation
4. Provide skillsets 7-9

#### README.md file should include the following items:

* Screenshot of Main/Splash Page
* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Screenshots of skillsets 7-9

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
>

#### Assignment Screenshots:
*Screenshot of lis4368/index.jsp:*
[<img src = "img/index.png">](http://localhost:9999/lis4368/index.jsp)

*Screenshot of Failed Validation*:

![JFailed Validation Screenshot](img/failed.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)

#### Skillset Screenshots:
| *Screenshot of SS7*:  | *Screenshot of SS9*: 
|---|---|
| ![SS7 Screenshot](img/ss7.png)  | ![SS9 Screenshot](img/ss9.png)  |   

*Screenshot of SS8*: 
 ![SS8 Screenshot](img/ss8edit.png) 