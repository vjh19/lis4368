<%@ page import="java.util.Date, java.util.TimeZone, java.text.SimpleDateFormat" %>
<nav class="navbar navbar-light navbar-fixed-top"style="background-color: #520A50;">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-m collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;"class="navbar-brand" href="index.jsp" target="_blank"><img src="img/ic_launcher.png" alt="Tomcat"/></a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="index.jsp">LIS4368</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a1/index.jsp">A1</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a2/index.jsp">A2</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a3/index.jsp">A3</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="customerform.jsp?assign_num=a4">A4</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="customerform.jsp?assign_num=a5">A5</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="p1/index.jsp">P1</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="customerform.jsp?assign_num=p2">P2</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="test/index.jsp">Test</a></li>					
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<%
//https://docs.oracle.com/javase/6/docs/api/java/text/SimpleDateFormat.html
SimpleDateFormat timeFormat = new SimpleDateFormat("M/d/yy h:m a ");
timeFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
String time = timeFormat.format(new Date());
%>
<%= time %>
