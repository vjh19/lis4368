> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applciations Development

## Victoria Hagen

### Assignment 3 Requirements:

*Deliverables:*

1. Create a entity Relationship Diagram (ERD) following the buisness rules:
    * A customer can buy many pets, but each pet, if purchased, is purchased by only one customer. 
    * A store has many pets, but each pet is sold by only one store. 
2. Include data (at leat 10 records each table)
3. Include ERD screenshot, a3.mwb, and a3.sql

#### README.md file should include the following items:

* Screenshot of ERD that link to image
* Skillsets 4-6


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
#### Assignment Screenshots:

*Screenshot of A3 ERD*:

![A3 ERD](img/a3.png "ERD based upon a3 Requirements")

*Screenshot of Petstore Table*:

![Petstore Table](img/petstore.png "10 entries")

*Screenshot of Pet Table*:

![Pet Table](img/pet.png "10 entries")

*Screenshot of Customer Table*:

![Customer Table](img/customer.png "10 entries")


*A3 docs: a3.mwb and a3.sql*: 
 
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format") 
 
[A3 SQL File](docs/a3.sql "A3 SQL Script") 

*Screenshot of a3/index.jsp:*
[<img src = "img/index.png">](http://localhost:9999/lis4368/a3/index.jsp)

#### Skillset Screenshots:
| *Screenshot of SS4*:  | *Screenshot of SS5*:  |  *Screenshot of SS6*: |
|---|---|---|
| ![SS4 Screenshot](img/ss4.png)  | ![SS5 Screenshot](img/ss5.png)  |    ![SS6 Screenshot](img/ss6.png)  |