
import java.util.*;
public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("1. Program swaps two user-entered floating-point values.");
        System.out.println("2. Create at least two user-defined methods: getRequirements() and numberSwap().");
        System.out.println("3. Must perform data validation: numbers msut be floats.");
        System.out.println("Print numbers before/after swapping.");

        System.out.println();
    }
    public static void numberSwap()
    {        
        Scanner sc = new Scanner(System.in);
   
     
        System.out.print("Enter num1: " );
    
        while(!sc.hasNextFloat()){
            
            System.out.println("Invalid input!");
            sc.next();
            System.out.print("\nPlease try again. Enter num1: ");
        }
        float num1 =sc.nextFloat();
        
        System.out.print("\nEnter num2: " );
    
        while(!sc.hasNextFloat()){
            
            System.out.println("Invalid input!");
            sc.next();
            System.out.print("\nPlease try again. Enter num2: ");
        }
        float num2 =sc.nextFloat();

        System.out.print("\nBefore swap: " );
        System.out.println("\nnum1 = "+num1+"\nnum2 = "+ num2);
       float temp=num1;
       num1=num2;
       num2=temp;

        System.out.print("\nAfter swap: " );
        System.out.print("\nnum1 = "+num1+ "\nnum2 = "+ num2 );
        

        

         //initialize variables, create Scanner object, capture user input
      
     sc.close();   
}

}
