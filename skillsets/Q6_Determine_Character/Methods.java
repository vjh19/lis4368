
import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program determines whether user-entered value is vowel, consonant, special character, or integer.\nProgram displays character's ASCII value");
        System.out.println();
        System.out.println("References: \nASCII Background: https://en.wikipedia.org/wiki/ASCII\nASCII Character Table: https://www.ascii-code.com/\nLookup Tables: https://www.lookuptables.com/");
        
        System.out.println();

    }
    public static void determineCharacter()
    {
 
         //initialize variables, create Scanner object, capture user input
        char value = 0;
        Scanner sc = new Scanner(System.in);
        
        for(int i=1; i>0; i++){
        System.out.println("//Run #"+i+":");
        System.out.print("Please enter character: ");
        value = sc.next().charAt(0);
        int ascii = (int)value;
        boolean alpha = Character.isAlphabetic(value);
        boolean numeric = Character.isDigit(value);
        boolean space = Character.isSpaceChar(value);
            if(alpha==true){
                switch (Character.toLowerCase(value)){
                    case 'a':
                    case 'e':
                    case 'i':
                    case 'o':
                    case 'u':
                    System.out.println(value+" is vowel. " +"ASCII value: "+ ascii);
                    break;
                    default:
                    System.out.println(value+" is consonant. " +"ASCII value: "+ ascii);
                }
            }
            if(numeric==true){
                System.out.println(value+" is integer. " +"ASCII value: "+ ascii);
            }
            if(numeric==false && alpha==false && space==false){
                System.out.println(value+" is special character. " +"ASCII value: "+ ascii);
            }
    System.out.println();   
    }
     sc.close();    
               
   }     
}

