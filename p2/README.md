> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications Development 

## Vicotria Hagen

### Project 2 Requirements:

*Three Parts:*

1.  Complete the JSP/Servlets web application using the MVC framework and providing 
create, read, update and delete (CRUD) functionality- adds the edit and delete buttons.
2. Compile the servlets.
3. Provide screenshots of the database and web pages for insert, update, and delete. 

#### README.md file should include the following items:

* Screenshots of valid user form entry (customerform.jsp ), passed validation (thanks.jsp), display data (customers.jsp).
*  Screenshots of modify form (modify.jsp)  and modified Data (customers.jsp).
* Screenshots of delete warning (customers.jsp) and associated database changes (select, insert, update, delete).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Valid User Form Entry (customerform.jsp)*:  | *Passed Validation (thanks.jsp)*: |*Display Data (customers.jsp)*: |
|---|---|---|
| ![Valid Entry](img/valid.png)  | ![Passed Validation](img/passed.png)  |  ![Display](img/displaydata.png) |

| *Modify Form (modify.jsp)*:  | *Modified Data (customers.jsp)*: |
|---|---|
| ![Modify](img/modify.png)  | ![Modified](img/modified.png)  | 

| *Delete Warning (customers.jsp) *:  | *Associated Database Changes (Select, Insert, Update, Delete) *: |
|---|---|
| ![Delete Warning](img/delete.png)  | ![Assocaited Tables](img/tables.png)  | 


