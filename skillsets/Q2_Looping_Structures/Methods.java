

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program loops through array of floats.\nUse following values: \nUse following loop structures: for, enhanced for, while, do...while.");
        System.out.println("\nNote: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");
        
        System.out.println();
    }
    public static void loopingStructure()
    {
 
         //initialize variables, create Scanner object, capture user input
      
        float[] val = {1.0f,2.1f,3.2f,4.3f,5.4f};
        int arraySize = val.length;
      
        System.out.println("for: ");
         for( int i=0; i<arraySize; i++){
            System.out.println(val[i]);
        }
        System.out.println();

        System.out.println("enhanced for: ");
        for(Float values: val)
        {
            System.out.println(values);
        }
        System.out.println();

        System.out.println("while: ");
        
        int f=0;
        while(f<arraySize){

            System.out.println(val[f]);
            f++;
        }
        System.out.println();

        int d=0;
        System.out.println("do...while: ");
        do{
            System.out.println(val[d]);
            d++;
        }while(d<arraySize);
        System.out.println();

}
}